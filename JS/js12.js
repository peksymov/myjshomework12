let imgContent = document.querySelectorAll('.image-to-show');
let currentImg = 0;
const hide = document.querySelectorAll('.image-to-show');
for (let i = 0; i < hide.length; i++) {
    hide[i].classList.add('hide');
}
let slider = setInterval(imgSlider, 2000);
function imgSlider() {
    imgContent[currentImg].classList.remove('current-img');
    currentImg = (currentImg + 1) % imgContent.length;
    imgContent[currentImg].classList.add('current-img');
}
const container = document.createElement('div');
container.classList.add('container');
document.querySelector('script').before(container);

const btnStop = document.createElement('button');
btnStop.classList.add('btn');
btnStop.innerHTML = 'Остановить показ';
container.append(btnStop);

btnStop.addEventListener('click', () => {
    clearInterval(slider);
});

const btnResume = document.createElement('button');
btnResume.classList.add('btn');
btnResume.innerHTML = 'Возобновить показ';
container.append(btnResume);
document.querySelector('script').before(container);

btnResume.addEventListener('click', () => {
    slider = setInterval(imgSlider, 2000);
});



